// Esfera.cpp: implementation of the Esfera class.
//
//Autor: Samuel Lopez Lopez
//Matricula: 52076
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=rand() %6 +2;
	velocidad.y=rand() %6 +2;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro.x=centro.x + velocidad.x*t;

	centro.y=centro.y + velocidad.y*t;

	//Funcionalidad extra: La pelota va disminuyendo su tamaño
        if(radio>0.2){
                radio=radio-0.1*t;
        }

}
