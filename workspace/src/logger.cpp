#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int main(){
	int t;
	char msj[60];
	mkfifo("/tmp/fifologger", 0777);
	t = open("/tmp/fifologger", O_RDONLY);
	while(read(t,msj,60)){
		printf("%s",msj);
		}
	close(t);
	unlink("/tmp/fifologger");
}
